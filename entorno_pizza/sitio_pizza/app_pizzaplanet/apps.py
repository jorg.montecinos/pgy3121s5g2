from django.apps import AppConfig


class AppPizzaplanetConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_pizzaplanet'
