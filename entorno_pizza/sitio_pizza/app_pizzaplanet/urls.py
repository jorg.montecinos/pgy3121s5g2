from django import urls
from app_pizzaplanet.serializers import ProdructoSerializer
from django.urls.conf import include
from app_pizzaplanet.models import carrito
from os import name
from django.urls import path
from .views import ProductoViewSet, miCarrito, editar, editarUser, eliminar, eliminar_usuario, gestionProductos, gestionVentas, index, ingresarProd,  listausuarios,   menuAdmin, miCarrito, modificarProd, pedidosDay,puntos,pedidos,locales,  registro
from rest_framework import routers

#API
router=routers.DefaultRouter()
router.register('gestionProductos', ProductoViewSet)

urlpatterns = [
    path('', index, name='index'),
    path('puntos/', puntos, name='puntos'),
    path('pedidos/',pedidos, name='pedidos'),
    path('locales/',locales, name='locales'),
    path('registro/', registro, name="registro"),
    path('menuAdmin/',menuAdmin,name='menuAdmin'),
    path('pedidosDay/',pedidosDay,name='pedidosDay'),
    path('gestiondeventas/',gestionVentas,name='gestionVentas'),
    path('GestionProductos/',gestionProductos,name='gestionProductos'),
    path('ingresarProd/',ingresarProd,name='ingresarProd'),
    path('modificarProd/',modificarProd,name='modificarProd'),
    path('listausuarios/',listausuarios, name='listausuarios'),
    path('eliminar/<id>',eliminar,name='eliminar'),
    path('editar/<id>',editar,name='editar'),
    path('editarUser/<id>',editarUser,name="editarUser"),
    path('eliminar_usuario/<id>',eliminar_usuario,name="eliminar_usuario"),
    path('miCarrito/',miCarrito,name="miCarrito"),
    path('api/',include(router.urls)), #api

]