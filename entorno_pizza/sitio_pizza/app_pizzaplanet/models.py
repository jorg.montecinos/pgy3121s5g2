from django.db import models

# Create your models here.

#modelo tabla de los productos
class Producto(models.Model):
    codigo=models.CharField(max_length=20,primary_key=True)
    nombre=models.CharField(max_length=30,null=False)
    descripcion=models.CharField(max_length=60,null=False)
    stock=models.CharField(max_length=30,null=False)
    valorUnitario=models.CharField(max_length=25,null=False)
    valorTotal=models.CharField(max_length=25,null=False)
    fechaIngreso=models.DateField(auto_now=True)
    imagen=models.ImageField(upload_to="fotos",null=True)


    def __str__(self):
        return f"{self.codigo} - {self.nombre} - {self.descripcion}-{self.stock} - {self.valorUnitario} - {self.valorTotal} - {self.fechaIngreso}"

class carrito(models.Model):
    usuario=models.CharField(max_length=100)
    codProducto=models.IntegerField()
