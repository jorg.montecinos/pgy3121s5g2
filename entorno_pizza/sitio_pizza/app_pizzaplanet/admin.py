from django.contrib import admin
from django.contrib.admin.filters import ListFilter
from django.db.models.base import Model
from app_pizzaplanet.models import Producto

# Register your models here.


class AdmProducto(admin.ModelAdmin):
    list_display=["codigo","imagen","nombre","descripcion","stock","valorUnitario","valorTotal","fechaIngreso"]
    list_filter=["nombre"]
    list_editable=["imagen","nombre","descripcion","stock"]



    class Meta:
        Model=Producto
admin.site.register(Producto,AdmProducto)




