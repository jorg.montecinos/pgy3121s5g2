class Localizacion{

    constructor(callback){
        if (navigator.geolocation){
            //obtener ubicacion
            navigator.geolocation.getCurrentPosition((position)=>{
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;

                callback();

            });
        }else{
            alert("no soporta geolocalizacion")
        }
    }
}

class Localizacion2{

    constructor(callback){
        if (navigator.geolocation){
            //obtener ubicacion
            navigator.geolocation.getCurrentPosition((position)=>{
                this.latitude = -36.82636633203377;
                this.longitude = -73.04774908946501;

                callback();
                
            });
        }else{
            alert("no soporta geolocalizacion")
        }
    }
}