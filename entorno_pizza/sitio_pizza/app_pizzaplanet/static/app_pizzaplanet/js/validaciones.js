
//FUNCION PARA LOS CAMPOS DE TEXTO
function sololetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

 // validacion contraseñas

 function validarpass() {

    var p2 = document.getElementById("psw2").value;
    var p1 = document.getElementById("psw1").value;

    if (p1 != p2) {

        document.getElementById("helpconf").innerHTML = "⚠ No coinciden las contraseñas";
        document.getElementById("helpconf").className = "form-text text-warning";


    }

    else {
        document.getElementById("helpconf").innerHTML = "✔ Contraseñas correctas";
        document.getElementById("helpconf").className = "form-text text-success";


    }

}

function validar() {

    p1 = document.getElementById("psw1").value;
    p2 = document.getElementById("psw2").value;
    var obj = document.getElementById("psw2");
    if (p1 != p2) {
        //alert("No");

        obj.setCustomValidity('Las contraseñas deben ser iguales');
        return false;
    }
    else {
        //alert("Si");
        obj.setCustomValidity('');
        return true;
    }
}
