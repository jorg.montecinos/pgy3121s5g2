function initMap(){ 
     
    const ubicacion2 = new Localizacion2(()=>{

        const myLatLng2 = { lat: ubicacion2.latitude, lng: ubicacion2.longitude }
        //sin la sgte linea no funciona, pero pq?...
        var texto2 = '<h5> Pizza Planet </h5>' + '<p> Libertador Gral. Bernardo OHiggins 825, Concepción, Bío Bío </p>' + '<a href="https://goo.gl/maps/aga9dADjL9iCpVeo6"> Abrir en Google Maps </a>'

        const options2 = {
            center: myLatLng2,
            zoom: 15
        }

        var map2 = document.getElementById('mapaLocal');

        const mapa2 = new google.maps.Map(mapaLocal, options2);

        const marcador2 = new google.maps.Marker({

            position: myLatLng2,
            map: mapa2,
            title: "Pizza Planet" 

        });

        var informacion2 = new google.maps.InfoWindow({
            content: texto2
        });

        marcador2.addListener('click', function(){
            informacion2.open(mapa2, marcador2);
        });

    });
};


window.addEventListener('load',function(){    
     
    const ubicacion = new Localizacion(()=>{

        const myLatLng = { lat: ubicacion.latitude, lng: ubicacion.longitude }
        //sin la sgte linea no funciona, pero pq?...
        //var texto = '<h5> Ubicacion Elegida </h5>' 

        const options = {
            center: myLatLng,
            zoom: 18
        }

        var map = document.getElementById('map');

        const mapa = new google.maps.Map(map, options);

        const marcador = new google.maps.Marker({

            position: myLatLng,
            map: mapa,
            

        });

        var informacion = new google.maps.InfoWindow();

        marcador.addListener('click', function(){
            informacion.open(mapa, marcador);
        });

        var autocomplete = document.getElementById('autocomplete');

        const search = new google.maps.places.Autocomplete(autocomplete);
        search.bindTo("bounds", mapa);
        
        search.addListener('place_changed', function(){

            informacion.close();
            marcador.setVisible(false);

            var place = search.getPlace();
            if(!place.geometry.viewport){
                window.alert("Error al mostrar el lugar");
                return;
            }
            if(place.geometry.viewport){
                mapa.fitBounds(place.geometry.viewport);
            }else{
                mapa.setCenter(place.geometry.location);
                mapa.setZoom(15);
            }
            marcador.setPosition(place.geometry.location);
            marcador.setVisible(true);

            var address = "";
            if (place.address_components){
                address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || ''),
                ];
            }

            informacion.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            informacion.open(map, marcador);
        });

    });
});

