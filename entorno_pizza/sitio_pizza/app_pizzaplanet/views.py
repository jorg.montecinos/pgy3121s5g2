from app_pizzaplanet.serializers import ProdructoSerializer
from django.contrib import auth
from django.db import reset_queries
from django.db.models import query
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404, redirect, render
from app_pizzaplanet.models import Producto
from app_pizzaplanet.forms import   formularioProducto, formularioRegistro
from django.contrib import messages # import necesario para mensaje alerta
from django.contrib.auth.models import User ##necesario para autenticar usuario
from django.contrib.auth import login, authenticate #necesario para autenticar usuario
from django.contrib.auth.decorators import login_required,permission_required
from rest_framework import viewsets



# Create your views here.

#API
from rest_framework import serializers
from .models import Producto
class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProdructoSerializer




#vista listado de usuarios en pagina del admin
@login_required #necesita login para entrar
@permission_required('auth.can_view_user') #permiso de admin para acceder
def listausuarios(request):
    users=User.objects.all()

    contexto={
        "usuarios":users
    }
    return render(request,"app_pizzaplanet/listaUsuarios.html",contexto)



###VISTAS USUARIO 

#vista del index
def index (request): 
    return render(request,"app_pizzaplanet/index.html")

#vista de planet puntos
@login_required
def puntos(request): 
    return render(request,"app_pizzaplanet/Planetpuntos.html")

#vista de mis pedidos
@login_required
def pedidos(request):
    return render(request,"app_pizzaplanet/historialPedidos.html")

#vista de locales
def locales(request):
    return render(request,"app_pizzaplanet/locales.html")

#vista para carrito de compras
@login_required
def miCarrito(request):
    return render(request,"app_pizzaplanet/carrito_compra.html")

#vista formulario registro de usuarios
def registro(request):
    form=formularioRegistro()

    if request.method=="POST":
        form=formularioRegistro(data=request.POST)
        if form.is_valid():
            form.save()
            #user y login son para autenticar el usuario
            user=authenticate(username=form.cleaned_data["username"],password=form.cleaned_data["password1"])
            login(request,user)
            messages.success(request, "Usuario Creado")
            return redirect(to="index")

    contexto={
        "form":form
    }

    return render(request,"registration/registro.html",contexto)




####VISTAS ADMINISTRADOR

#vista menu administrador
@login_required
@permission_required('auth.can_view_user')
def menuAdmin(request):
    return render(request,'app_pizzaplanet/menuadmin.html')

#vista pedidos de hoy
@login_required
@permission_required('auth.can_view_user')
def pedidosDay(request):
    return render(request,'app_pizzaplanet/AdminPedidosDelDia.html')

#vista gestion de ventas
@login_required
@permission_required('auth.can_view_user')
def gestionVentas(request):
    return render(request,'app_pizzaplanet/Admin_gestionventas.html')

#vista gestion de productos
@login_required
@permission_required('auth.can_view_user')
def gestionProductos(request):
    
    productos=Producto.objects.all()

    contexto={
        "productos":productos
    }
    return render(request,'app_pizzaplanet/adminGestionProductos.html',contexto)


#vista ingresar producto + alerta
@login_required
@permission_required('auth.can_view_user')
def ingresarProd(request):
    formulario=formularioProducto(request.POST or None)

    contexto={
        'form':formulario
    }
    
    if request.method=="POST":
        form=formularioProducto(data=request.POST,files=request.FILES)
        
        if form.is_valid():
            form.save()
            messages.success(request, "Producto ingresado correctamente")
            return redirect(to="gestionProductos")
            
    return render(request,'app_pizzaplanet/ingresarProducto.html',contexto)


#vista de modificar producto
@login_required
@permission_required('auth.can_view_user')
def modificarProd(request):
    Productos=Producto.objects.all()
    contexto={
        'productos':Productos

    }
    return render(request,'app_pizzaplanet/modificarProducto.html',contexto)




    formulario2=formularioAdmin(request.POST or None)

    contexto={
        
        'form2':formulario2
    }
    
    if request.method=="POST":
        if formulario2.is_valid:
            formulario2.save()
            messages.success(request,"Se ha registrado nuevo administrador")
            return redirect(to="listaAdmin")

            
          

    return render(request,'app_pizzaplanet/nuevoAdmin.html',contexto)







#FUNCIONES ELIMINAR Y EDITAR GESTION DE PRODUCTOS, LISTADO DE ADMIN

#eliminar producto + alerta
@login_required
@permission_required('auth.can_view_user')
def eliminar(request,id):

    prod=get_object_or_404(Producto,codigo=id)
    prod.delete()
    messages.success(request,"El producto ha sido eliminado")
                
    return redirect(to="gestionProductos")



#editar producto + alerta
@login_required
@permission_required('auth.can_view_user')
def editar(request,id):
    prod=get_object_or_404(Producto,codigo=id)
    formulario=formularioProducto(instance=prod)
  
    

    contexto={
        'form':formulario
    }
    

    if request.method=='POST':
        formulario=formularioProducto(data=request.POST,files=request.FILES,instance=prod)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"El producto ha sido modificado")
            return redirect(to="gestionProductos")
        
        contexto={
            "form":formulario
        }
    return render(request,'app_pizzaplanet/editar.html',contexto)



#eliminar desde listado admin + alerta
@login_required
@permission_required('auth.can_view_user')
def eliminar_usuario(request,id):
    user=get_object_or_404(User,id=id)
    user.delete()

    messages.success(request,"eliminado correctamente")
    return redirect(to="listausuarios")


#editar desde listado de usuarios + alerta
@login_required
@permission_required('auth.can_view_user')
def editarUser(request,id):
    usuario=get_object_or_404(User,id=id)
    formulario=formularioRegistro(instance=usuario)
  
    

    contexto={
        'form':formulario
    }
    

    if request.method=='POST':
        formulario=formularioRegistro(data=request.POST,files=request.FILES,instance=usuario)
        if formulario.is_valid():
            formulario.save()
            messages.success(request,"Usuario modificado")
            return redirect(to="listausuarios")
        
        contexto={
            "form":formulario
        }
    return render(request,'app_pizzaplanet/editarUsuarios.html',contexto)