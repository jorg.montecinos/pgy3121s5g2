from django import forms
import django
from django.db.models.base import Model
from django.forms import fields
from .models import Producto
from django.contrib.auth.forms import UserCreationForm #import para creacion de usuarios
from django.contrib.auth.models import User #importo la tabla de usuario



#formulario para registro de usuarios
class formularioRegistro(UserCreationForm):

    class Meta:
        model=User
        fields=["username","email","first_name","last_name"]




#formulario para ingresar un producto
class formularioProducto(forms.ModelForm):
    #fechaIngreso=forms.DateField(label="Fecha ingreso", widget=forms.TextInput(attrs={'type':'date'}))

   
    class Meta:
        model=Producto
        fields=["codigo","nombre","descripcion","stock","valorUnitario","valorTotal","imagen"] 








